﻿using GTA;
using System;
using System.IO;
using System.Windows.Forms;

namespace ForceFieldPlusPlus
{
    class Sphere
    {
        int sphere = -99999;
        public Sphere(Vector3 position, float radius)
        {
            try
            {
                sphere = GTA.Native.Function.Call<int>("ADD_SPHERE", position.X, position.Y, position.Z, radius, 1);
            }
            catch (Exception)
            {
                sphere = -99999;
            }
        }

        public bool Exists()
        {
            if (sphere == -99999) return false;
            return true;
        }

        public void Remove()
        {
            if (sphere != -99999)
            {
                GTA.Native.Function.Call("REMOVE_SPHERE", sphere);
                sphere = -99999;
            }
        }
    }


    public class ForceFieldPlusPlus_Script : Script
    {
        GTA.Timer timer1;
        Sphere sphere1, sphere2;
        Keys holdKey, pressKey;
        bool enabled = false;
        float radius = 20.0f;
        float force = 150.0f;
        uint sphere1or2 = 1;
        bool useSphere;

        public ForceFieldPlusPlus_Script()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("radius", 20.0f);
                Settings.SetValue("force", 150.0f);
                Settings.SetValue("use_sphere", true);
                Settings.SetValue("toggle_hold_key", Keys.RControlKey);
                Settings.SetValue("toggle_press_key", Keys.K);
                Settings.Save();
            }

            radius = Settings.GetValueFloat("radius", 20.0f);
            force = Settings.GetValueFloat("force", 150.0f);
            useSphere = Settings.GetValueBool("use_sphere", true);

            holdKey = Settings.GetValueKey("toggle_hold_key", Keys.RControlKey);
            pressKey = Settings.GetValueKey("toggle_press_key", Keys.K);

            timer1 = new GTA.Timer(200, false);
            timer1.Tick += timer1_Tick;
            timer1.Start();

            if (useSphere)
            {
                PerFrameDrawing += ForceFieldPlusPlus_Script_PerFrameDrawing;
            }

            KeyDown += ForceFieldPlusPlus_Script_KeyDown;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (enabled)
            {
                Vector3 pos = Player.Character.Position, direction;
                Vehicle[] vehicles = World.GetVehicles(pos, radius);
                Ped[] peds = World.GetPeds(pos, radius);
                

                if (vehicles != null)
                {
                    foreach (Vehicle v in vehicles)
                    {
                        if (Game.Exists(v))
                        {
                            if (Player.Character.CurrentVehicle == null || v != Player.Character.CurrentVehicle && !Player.Character.isTouching(v))
                            {
                                direction = Vector3.Normalize(v.Position - Player.Character.Position) * force;
                                direction.Z = force - force / 3.0f;
                                v.ApplyForce(direction);
                            }
                        }
                    }
                }

                if (peds != null)
                {
                    foreach (Ped p in peds)
                    {
                        if (Game.Exists(p) && !p.isDead && !p.isInVehicle() && p != Player.Character && !Player.Character.isTouching(p))
                        {
                            direction = Vector3.Normalize(p.Position - Player.Character.Position) * force;
                            direction.Z = force - force / 3.0f;
                            p.ForceRagdoll(3000, false);
                            p.ApplyForce(direction);
                        }
                    }
                }
            }
            else
            {
                if (sphere1 != null && sphere1.Exists())
                {
                    sphere1.Remove();
                }

                if (sphere2 != null && sphere2.Exists())
                {
                    sphere2.Remove();
                }
            }
        }

        private void ForceFieldPlusPlus_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(holdKey) && e.Key == pressKey)
            {
                enabled = !enabled;
            }
        }

        private void ForceFieldPlusPlus_Script_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (enabled)
            {
                if (sphere1or2 == 1)
                {
                    Vector3 pos = Player.Character.Position;
                    sphere1 = new Sphere(new Vector3(pos.X, pos.Y, pos.Z - 5.0f), (radius * 2));

                    if (sphere2 != null && sphere2.Exists())
                    {
                        sphere2.Remove();
                    }

                    sphere1or2 = 2;
                }
                else if (sphere1or2 == 2)
                {
                    Vector3 pos = Player.Character.Position;
                    sphere2 = new Sphere(new Vector3(pos.X, pos.Y, pos.Z - 5.0f), (radius * 2));

                    if (sphere1 != null && sphere1.Exists())
                    {
                        sphere1.Remove();
                    }

                    sphere1or2 = 1;
                }
            }
        }
    }
}
